<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>

        <title>Icarus &#8211; File Upload</title>

        <link rel="icon" href="images/favicon.ico" type="image/x-icon">

        <link rel="stylesheet" media="all" href="css/main.css">
        <link rel="stylesheet" media="all" href="css/fontello.css">
    </head>
    <body>
        <div id="header">
            <div class="wrapper">
                <div class="row">
                    <div class="col-l-10 col-m-10 col-s-12">
                        <h1 class="page-header">{ Icarus } <span class="small">by Creased</span></h1>
                    </div><!--
                    --><div id="nav" class="col-l-2 col-m-2 col-s-12">
                        <ul>
                            <li><a href="index.php">Home</a></li><!--
                            --><li><a href="upload.php">Upload</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
