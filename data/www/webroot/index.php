<?php
    require_once("include/header.php");
?>
<div id="main">
    <div class="wrapper">
        <div class="row">
            <div class="col-l-12 col-m-12 col-s-12">
                <h2>Welcome to Icarus!</h2>
                <hr />
                <p>A powerful file upload service</p>
                <a href="upload.php" id="main-button" title="upload">Get started</a>
            </div>
        </div>
    </div>
</div>
<?php
    require_once("include/footer.php");
