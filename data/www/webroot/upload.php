<?php
    function basicRandomString($length = 15) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    require_once("include/header.php");
?>
        <div id="main">
            <div class="wrapper">
                <div class="row">
                    <div class="col-l-12 col-m-12 col-s-12">
<?php
    if ((isset($_POST)) && ($_SERVER['REQUEST_METHOD'] == 'POST')) {
?>
                        <p>
<?php
        // Process POST request
        if (isset($_FILES['file'])) {
            if ($_FILES['file']['error'] == 0) {
                $file_name = $_FILES['file']['name'];  // Get original filename
                $file_size = $_FILES['file']['size'];  // Get filesize
                $file_temp = $_FILES['file']['tmp_name'];  // Get temporary filename
                $file_info = pathinfo($file_name);  // Array containing filename info
                $file_extension = strtolower($file_info["extension"]); // Get image extension from filename
                $new_file_name = basicRandomString() . '.' . $file_extension;  // Generate a new random filename (keep lowercase extension)

                if (isset($_POST['upload']) && is_dir($_POST['upload'])) {  // Upload directory
                    $upload_dir = $_POST['upload'];
                } else {
                    $upload_dir = 'uploads';
                }

                if ($file_size < 104857600) {  // If filesize is lower than 100MB
                    if (move_uploaded_file($file_temp, $upload_dir . '/' . $new_file_name)) {  // Move file to upload directory
                        echo('Your file has successfully been uploaded to <a href="' . $upload_dir . '/' . $new_file_name . '" title="upload file">'. $upload_dir . '/' . $new_file_name . '</a>.');
                    }
                } else {
                    header('400 Bad Request', true, 400);
                    echo('Your file exceeds file size limit of 100 MB!');
                }
            } else {
                header('500 Internal Server Error', true, 500);
                echo('An error occured while processing your file!');
            }
        } else {
            header('400 Bad Request', true, 400);
            echo('Please send a file!');
        }
?>
                               </p>
<?php
    } elseif ((isset($_GET)) && ($_SERVER['REQUEST_METHOD'] == 'GET')) {
?>
                        <form action="upload.php" method="POST" enctype="multipart/form-data">
                            <fieldset>
                                <input type="file" name="file" required="required" />
                                <input type="hidden" name="upload" value="uploads" />
                            </fieldset>
                            <fieldset>
                                <input type="submit" value="Upload" />
                            </fieldset>
                        </form>
<?php
    } else {
        // Method Not Allowed
        header('Method Not Allowed', true, 405);
        echo('Method Not Allowed');
    }
?>
                    </div>
                </div>
            </div>
        </div>
<?php
    require_once("include/footer.php");

