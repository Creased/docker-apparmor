#!/bin/bash

###
# nginx
#
nginx -g "daemon on;"

##
# logs
#
tail -F /usr/share/nginx/log/*.log
